# Floating Rotary Broach Tool

This rotary broach is intended to be simple to make, with only
three parts requiring machining, and tolerant of different lengths
of broach by allowing the broach to float while still holding it
at a precise 1° angle relative to the shaft centerline. The float
allows the broach axis to meet the tool axis at a range of locations.

The theory of operation is that the nut and the body hold two axial
bearings which hold the spindle, and the body bore is wider than
the bearings and spindle, which allows it to float radially while
constraining the 1° angle that produces the "wobble" motion that
drives the broach into the workpiece.

The float makes it typically **required** to start with a chamfer to
locate the tip of the broach relative to the hole. To successfully
broach without a chamfer, a different tool that precisely locates
the broach tip relative to the tool axis is required. Such a tool
requires broaches of a precise length, or adjustability to accommodate
different broach lengths.

This tool is modeled (initially using FreeCAD 0.22 development
versions) in multiple configurations which are driven by a FreeCAD
configuration table. Drawings show one example configuration, but
others are included in the model, and the configuration table can
be extended.

The default model holds 8mm stock (or commercial 8mm / 0.315"
broaches such as those available from Polygon Solutions or McMaster)
and uses F12-21M thrust bearings. These have a 12mm ID, 21mm OD,
are 5mm thick, and are rated to 1800RPM / 2600N (584 pound-force)
dynamic load. The drawings and this text support this configuration.
Note, however, that the set screw is not aligned with the flats on
commercial broaches.

The outside of the tool body follows the 1° broach axis, not the
machine axis. This means that it will "wobble" visibly if used in
a mill. The bearings limit this broach to a design speed of 1800RPM.

Some of the variants use thrust needle bearings rated for higher
speeds and loads. However, thrust ball bearings are sufficient for
the expected speeds and loads for this tool.

## BOM

* 1 M4 set screw
* 2 F12-21M thrust bearings
* 0.75" steel stock, at least 24mm 
* 1.125" steel stock, at least 55mm plus work holding
* 1 steel shaft, 2-3" long, machinist's choice (tough or hardened steel recommended)
* 8mm precision tool steel (e.g. O1, A2, W1) for broaches

## Machining Notes

Seasoned machinists should be able to make these parts from the
drawings. However, this design is intended to be accessible to
hobby machinists, who may appreciate notes on one approach.

These note assume the default configuration. Adjust as appropriate
for other configurations.

### Body

Use 1.125" stock.

Face, then turn to 25mm (.984") to 6.0mm (.236").

Bore ø22.0mm (.866") to 22mm (.866") depth, to a sharp corner.
(Consider boring using an end mill as a boring bar to get a sharp
shoulder.) The bore depth should be precise; boring 0.1-0.2mm too
deep and then facing to length may be the easiest way to bring to
precise depth.

Turn front and back thread relief to 24.24mm (.954"). Single-point
thread 0.5mm pitch.

Part slightly over length, then face to 34.0mm (1.34") total length.
(This can be done with either a lathe or a mill.)

To prepare the socket for the shaft, set the body, bore down, in
the vise, held in a V-block for a firmer grip, with a 1° angle
block under it or otherwise set it at a precise 1° angle, with
the high side to the **right**. Note that the V-block will also
be at a 1° angle.  If this setup slips, you may easily scrap
the part. Consider holding the part with no-slip sandpaper or
double-sided tape to make it more secure.

Mark the rear side for locating the set screw hole later.

Locate the center of the part accurately. A center indicator or
Indicol-style DTI holder is appropriate here.  Then offset 0.77mm
(0.03") **towards** the **higher** edge on the **right**, and
**away** from the **lower** edge.

To reduce cutting forces, use successive drills and then bore
small increments, as this is a relatively delicate setup, and
there is no precise reference from which to pick up this setup
should the part slip.

Drill through into the existing body cavity with any small drill
to make a hole for pressure relief while fixing the shaft. The
location and size are not critical.

Do not break through into the existing body cavity while boring to
shaft diameter. The inside face supports a bearing race. Measure the
shaft, and hold appropriate relative tolerances for the retaining
compound you plan to use. Do not press fit the shaft; the body is
too thin, and the 1° angle will make it difficult anyway. The shaft
will not carry any substantial radial load, so an interference fit
is unnecessary. If you do not have retaining compound, a close fit
and thread locker ("loctite") will suffice.

Finally, a hole in the side of the body is needed to access
the broach set screw in the spindle. Locate the face that was
previously against the rear vise jaw facing up. Clamp the shaft in
the mill vise either using a collet block or front to back with the
faces against the vise jaws and drill a hole roughly 6mm or 1/4",
centered 10.75mm (.42") from the front face.

### Nut

Use the same stock as the Body.

Bore ø12.50mm (.492") to more than 8mm (.315") deep.

Bore ø24.46mm (.963") to 6.0mm (.236") depth.

Turn runout area ø25mm (.984") 2mm depth to ø25mm inside nut.

Thread 0.5mm pitch. Test thread fit against body. When fully screwed
in, the inner nut face should touch the end of the body face. Part
off to length or slightly long.

If parted long, screw nut onto body, and take light facing passes
to bring nut to length. (Excess length here will only limit total
broach depth; this dimension is otherwise not critical.)

You may cut flats as shown, or cut pockets of the same depth for a
pin spanner, or cut neither. Rubber bands wrapped around the nut
give sufficient grip for initial adjustment. Features are only
to break loose loctite for cleaning, and it's not clear that they
are necessary.

## Spindle

Use 0.75" stock.

Face, then drill a small hole (~2mm or ~1/16") at least 5mm deep
and up to full lenth.

Turn 12mm (.472") tested to slip fit the **smaller** of the two
bearing races (they are not quite the same size), 4.5mm (.177") deep.

Test that this captures the whole bearing stack, with the end
slightly (0.5mm) recessed into the bearing stack. Test with .001"
shim stock that the bearing seats firmly against the spindle
shoulder; the inside shoulder should have a smaller radius than
the radius of the smaller bearing race.

If you use larger than 0.75" stock, turn down to 19mm or 0.75" 24mm+
(~1") long.

Part off over size. Flip part end for end.

Face to length.

Turn 12mm (.472") 7.00mm (.276") deep.

Bore to ø8mm precisely, 20mm (.788") deep.

You may choose to grind a flat (180°) drill bit to create
a flat seat. I spot drill, pilot drill, drill 9/32" with a normal
drill to depth, drill with a 9/32" twist drill ground flat to depth,
drill 7.9mm to depth, then ream 8mm. This should be a "piston fit"
with 8mm drill rod.

After reaming, ensure that the smaller bearing race fits over the
12mm shoulder, as with the other end.

Clamp the spindle front-to-back in the mill vise, find the center
in X, then drill 3.3-3.4mm and tap M4 13.25mm (.52") from the front
end of the spindle (with the larger hole and longer 12mm turned
face).

### Variation

This design can accomodate 5/16" drill rod, which is more readily
accessible in the US than 8mm drill rod. If you plan to make all
your own broaches, this difference will not matter.  Merely bore
the spindle 5/16" instead of 8mm.

### Assembly

Use retaining compound to hold the shaft in the back of the body,
and wait 24 hours for it to cure. Make sure to clean any retaining
compound that gets inside the body. (Retaining compound inside
the body will freeze the rear bearing in place.)

Put a short piece of 8mm drill rod in the spindle, and lock it in
place with an M4 grub screw.

Hold the spindle with the nose down, and put the **large** race of
one bearing on the back of the spindle, with the raceway up. Put a
drop of light oil or dab of lithium grease in the raceway, and add
the bearing cage. Put the smaller raceway on top of the bearing
cage, raceway down. Add a few drops of oil or coating of grease
to the **back** side of the smaller raceway. Set the body over
the spindle until the bearing bears firmly against the bottom,
with the ball bearings engaged in both raceways. (Note that
the arrangement of large and small raceways is opposite normal
practice. This arrangement is to leave room for an imperfectly
square inside corner, and is otherwise unimportant.)

Invert the body and spindle while holding them together, to keep
the balls in the cage engaged with the bearing seat raceways.

Put the large race of the other bearing set on the exposed face of
the spindle. Again add a drop of light oil, add the cage, set the
smaller raceway on top with the ball bearings engaged, and oil the
top of the smaller raceway.

For permanent assembly, add just a drop of blue loctite to the body
threads. (This should be easy to remove if you need to clean swarf
out of the bearings, but not loosen from vibration in use.)

Screw the nut on, testing that the bearings are still seated.
The nose of the spindle should be coplanar with the nut face.
Tighten the nut until the spindle no longer floats, then back it
off slightly only until the spindle just barely floats smoothly,
with no angular play. You can wrap a rubber band around the nut
for better grip if necessary while adjusting the nut. Wait for the
loctite to set.

Rotate the spindle to align the screw with the access hole, and
remove the drill rod from the spindle.

Wait for retaining compound and loctite to cure before use.

## Cutting a broach

Start with 8mm precision ground drill rod.

You may choose to dish the front to reduce cutting pressure.  (It is
not necessary to make this rounded.)  If you will be broaching soft
metals, you can do this with a spotting drill in the lathe. If you
will be broaching steel, you may want to use the compound to dish
at a much shallower 1–3° angle to reduce wear on the broach.

You may also drill a small hole down the
middle of the length of the shaft for pressure relief; this
is useful if you drill smaller holes to avoid exposed drill
radius on the surface of the drilled hole. (The hole at the
back of the spindle is to enable this pressure relief. it
vents through the bearings and set screw access hole.)

Cut at a 2.5° angle. This will give a clearance angle of 1.5°
when the broach is in use, after subtracting the 1° wobble axis
angle. You can do this by putting the stock in a universal dividing
head and setting the head at 2.5° down. Alternatively, you can
make a repeatable fixture for your mill vise to hold a collet at
a repeatable station and 2.5° down angle, and turn the collet to
make the cuts.

The general rule of thumb is that the depth of cut should be 1.5x
the target size across flats. You may be able to go as high as 2x
the nominal size, but I recommend starting conservative for your
first broaches. Use a sharp end mill and light cuts to minimize
deflection and get a sharp edge.

Measure your cuts across the flats. Cut the broach 50-100 microns
(.002"-.004") oversize. In most cases, you want a loose sliding
fit for your wrenches, and you will need some room for sharpening
and honing the broach after you harden it.

Part the broach to length. In fixed broach holders, the length
is important to hold tightly. In this floating broach holder,
the length needs to be between the minimum and maximum design
length.

Harden and temper the broach as appropriate for the chosen grade
of tool steel. Hone the edges.

## Configuration Changes

Two kinds of re-configuration are available: Using different
bearings and broach sizes, and changing individual parameters
not specific to the bearings or broach sizes.

In FloatingRotaryBroach.FCStd, all the parameters are kept
in the **p** (for "parameters") spreadsheet. This includes
the configuration table and multiple individual parameters.

### Bearings / Broach dimensions

In FreeCAD, open the FloatingRotaryBroach.FCstd file, select the
Broach object, and change the Size configuration, which is named
first by the broad diameter (e.g. 8mm) and second by the bearing
type (e.g. F12-21M).

To add an additional configuration, you can add a row to the
configuration table. It is easy to create nonsense in adding a
row. Study the sketches that drive the rotations at the beginning
of the Nut, Body, and Spindle parts to understand the limits.
Some impossibilities will show as failures to compute, but not
all.

You can open FloatingRotaryBroachAssembly3.FCStd in FreeCAD with the
Assembly3 workbench installed to review whether a new configuration
can fit together. With both of the FreeCAD files open and the
assembly open with the Assembly3 workbench active, change the Size
configuration for the Broach part in the FloatingRotaryBroach.FCStd
file, then in the assembly, the hot key sequence **A S** will solve
the assembly for your new configuration, and you can review it for
whether it makes sense.

After any changes, open the BodyPage, NutPage, and SpindlePage
TechDraw Pages with the TechDraw workbench active, and modify the
drawings to have correct and sufficient dimensions. Note that as
of the time of writing, the TechDraw workbench is substantially
affected by topological naming instability (the "Topological Naming
Problem") and you will want to check all dimensions carefully
after reconfiguration. Look for warnings on dimensions, as invalid
dimensions will show as zero length.

Alternatively, you can create new drawings that you can interpret.

### Individual parameters

The **Input Parameters** section has dimensions intended to be
directly changed (within reason), and are marked in Bold. (Additional
*Calculated Parameters* in italic should not be changed unless
you really want to modify the calculation, but the dependencies of
these parameters may not be obvious in every case.)

The model is made to assume some precision 1/2" stock for the
shaft. Cutting the threads off a damaged 1/2" endmill is one way to
get this. To visualize other size shafts, change the **ShaftDiameter**
input parameter.
